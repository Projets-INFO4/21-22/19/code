


int main(){
    int len = 10;
    int array[10] = {3, 2, 7, 9, 10, 1, 5, 4, 6, 8};
    int idx_min;
    int min;
    int tmp;
    int k;
    for (k = 0 ; k < len ; k ++){
        idx_min = k;
        min = array[k];
        for (int l = k ; l < len ; l ++){
            if (array[l] > min){
                min = array[l];
                idx_min = l;
            }
        }
        tmp = array[k];
        array[k] = array[idx_min];
        array[idx_min] = tmp;
    }

    for (k = 0 ; k < len ; k ++){
        if (k+1 != array[k]){
            return 0;
        }
    }
    return 1;
}