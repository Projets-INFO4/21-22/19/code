
int pow(int x, int n) {
    if (n == 1){
        return x*x;
    }
    return pow(x, n-1);
}

int main(){
    int x = 3;

    if (pow(3, 3) == 27){
        return 1;
    }
    return 0;
}

