# MCPU emulator, see https://github.com/cpldcpu/MCPU
#   python asm.py <... | python emu.py
# optional arg is # of cycles to run
# show instruction trace if cycles < 1000

from __future__ import print_function
import sys
import numpy as np

try:
    cycles = int(sys.argv[1]) # argv[1] : code machine pour le mcpu 
except:
    cycles = 20

size_mem = 2**21
mem = np.zeros(shape=size_mem, dtype=np.uint32)

with open(sys.argv[1], 'r') as f1:
    for i, line in enumerate(f1.readlines()):
        mem[i] = int(line.strip(), 16)

param_offset = mem[1]
input_offset = mem[2]
stop_offset = mem[3]
output_offset = size_mem - 1
mem[param_offset] = output_offset # définition de l'output offset

with open(sys.argv[2], 'r') as f2:
    i = 0
    for line in f2.readlines():
        for ch in line:
            mem[input_offset + i] = int(ord(ch))
            i += 1

    mem[param_offset + 1] = i # définition de la taille de l'input

pc = 0
acc = 0
cf = 0

for _ in range(cycles):
    ir = mem[pc & 0x3FFFFFFF]
    pc += 1
    op, arg = ir >> 30, ir & 0x3FFFFFFF

    if cycles < 1000:
        print('pc,ir,acc,cf:', pc-1, hex(ir), acc, cf >> 32, sep='\t')

    if op == 0:
        acc = (acc | mem[arg]) ^ 0xFFFFFFFF
    elif op == 1:
        acc += mem[arg]
        cf = acc & 0x100000000
        acc &= 0xFFFFFFFF
    elif op == 2:
        mem[arg] = acc
    else:
        if ir == 0xFFFFFFFF:
            print(acc)
        elif cf == 0:
            pc = arg
        cf = 0

# mettre dans un fichier (3e argument) l'output
size_output = mem[output_offset]

output_file = open(sys.argv[3], "w")
for i in range(-1, -size_output-1, -1):
    output_file.write("%0.8X\n" % mem[output_offset+i])
