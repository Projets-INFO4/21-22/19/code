
int outSize = 1048576; //2M
int OUTPUT[1048576];

int inputSize = 0;
char* inputStr = "abc";

int read(int offset){
    return inputStr[offset];
}
int write(int value, int offset){
    OUTPUT[outSize - 2 - offset] = value;
}
int peekOutput(int offset){
    return OUTPUT[outSize - 2 - offset] & 1073741823;
}
int edit(int ptr, int offset){
    int val = OUTPUT[outSize - 2 - offset] & 3221225472;
    val = val | ptr & 1073741823;
    OUTPUT[outSize - 2 - offset] = val;
}


// (val)
int constants[16384];
int constantPtr=0;

int findConstant(int val){
    int i=0;
    while(i < constantPtr){
        if(constants[i] == val){
            return i;
        }
        i=i+1;
    }
    return -1;
}
int addConstant(int val){
    constants[constantPtr] = val;
    constantPtr=constantPtr+1;
    return constantPtr-1;
}

int getConstant(int val){
    int ptr;
    if((ptr = findConstant(val)) == -1){
        return addConstant(val);
    }
    return ptr;
}

// (id, size, type, addr)
int globals[4096];
int globalPtr = 0;

int findGlobal(int id){
    int i=0;
    while(i < globalPtr){
        if(globals[i] == id){
            return i;
        }
        i=i+4;
    }
    return -1;
}
int addGlobal(int id, int size, int type){
    int r = globalPtr;
    globals[globalPtr] = id;
    globalPtr=globalPtr+1;
    globals[globalPtr] = size;
    globalPtr=globalPtr+1;
    globals[globalPtr] = type;
    globalPtr=globalPtr+1;
    globals[globalPtr] = 0;
    globalPtr=globalPtr+1;

    return r;
}
int globalSize(int ptr){
    return globals[ptr + 1];
}

//types
int TPINT = 1;
int TPSTR = 2;

// (id, paramOffset, nbParams, code)
int functions[4096];
int functionsPtr = 0;

// (type, id)
int functionParams[4096];
int funcParamPtr = 0;

int findFunction(int id){
    int i=0;
    while(i < functionsPtr){
        if(functions[i] == id){
            return i;
        }
        i=i+4;
    }
    return -1;
}

int addFunction(int id){
    int r = functionsPtr;
    functions[functionsPtr] = id;
    functionsPtr = functionsPtr + 1;
    functions[functionsPtr] = funcParamPtr;
    functionsPtr = functionsPtr + 1;
    functions[functionsPtr] = 0;
    functionsPtr = functionsPtr + 1;
    functions[functionsPtr] = -1;
    functionsPtr = functionsPtr + 1;

    return r;
}

// (id, offset, type)
int variables[768];
int variablePtr = 0;

int findVariable(int id){
    int i = variablePtr-3;
    while(i >= 0){
        if(variables[i] == id){
            return i;
        }
        i = i-3;
    }
    return -1;
}

int varSize(int varPtr){
    return variables[variablePtr+4] - variables[variablePtr+1];
}

int pushVariable(int id, int size, int type){    
    variables[variablePtr] = id;
    variablePtr = variablePtr+1;

    if(variablePtr == 1){
        variables[variablePtr] = 0;
    }
    variables[variablePtr+3] = variables[variablePtr] + size;
    
    
    variablePtr = variablePtr+1;
    variables[variablePtr] = type;
    variablePtr = variablePtr+1;
    return variablePtr-3;
}

int restoreStack(int ptr){
    variablePtr = ptr;
}

int setFunctionNbParam(int funcPtr, int nbParam){
    functions[funcPtr+2] = nbParam;
}
int setFunctionCode(int funcPtr, int line){
    functions[funcPtr+3] = line;
}
int addFunctionParam(int type, int id){
    functionParams[funcParamPtr] = type;
    funcParamPtr=funcParamPtr+1;
    functionParams[funcParamPtr] = id;
    funcParamPtr=funcParamPtr+1;
}

int charCount = 0;
int lineCount = 0;
int lineCharCount = 0;

int nl=0;


int get(){
    int c = inputStr[charCount];
    charCount=charCount+1;
    lineCharCount=lineCharCount+1;
    
    if(nl == 1){
        nl = 0;
        lineCharCount = 0;
        lineCount=lineCount+1;
    }
    if(c==10){
        nl = 1;
    }
    return c;   
}

char* keywords = "int\0char\0if\0else\0while\0return";

int KW_INT = 0;
int KW_CHAR = 1;
int KW_IF = 2;
int KW_ELSE = 3;
int KW_WHILE = 4;
int KW_RETURN = 5;

int NB_KW = 6;

int IDs[2048];
int IDOffset[256];
int IDArrayPtr = 0;

int strings[16384];
int strOffset[256];
int strArrayPtr = 0;
int strSize = 0;

// === lexer buffer managment ===
int readBuffer[128];
int readBufferIndex = 0;

int readBufferAddChar(int c){
    readBuffer[readBufferIndex] = c;
    readBufferIndex = readBufferIndex+1;
}
int resetReadBuffer(){
    readBufferIndex = 0;
}
// ==============================


// === lexer util functions === 
int alpha(int c){
    return (c >= 65) & (c <= 90)
         | (c >= 97) & (c <= 122)
         | c == 95;
}
int num(int c){
    return (c >= 48) & (c <= 57);
}
int alphanum(int c){
    return alpha(c) | num(c);
}
int blank(int c){
    return c == 32 | c == 9 | c == 10 | c == 13;
}

int eof(int c){
    return c == -1;
}

int quotes(int c){
    return c == 34;
}

int slash(int c){
    return c == 47;
}
int lf(int c){
    return c == 10;
}


// ============================


// === token types ==
int TID = 1;
int TCONST = 2;
int TTERM = 3;
int TSTR = 4;
int TKW = 5;
int TEND  = 6;
int TERR = 7;


// terminals


int TM_SC = 1;
int TM_CM = 2;
int TM_LP = 3;
int TM_RP = 4;
int TM_LB = 5;
int TM_RB = 6;
int TM_LC = 7;
int TM_RC = 8;
int TM_PL = 9;
int TM_MN = 10;
int TM_OR = 11;
int TM_ND = 12;
int TM_EQ = 13;
int TM_NE = 14;
int TM_LT = 15;
int TM_GT = 16;
int TM_LE = 17;
int TM_GE = 18;
int TM_NT = 19;
int TM_CP = 20;
int TM_ST = 21;
int TM_AS = 22;

int TM_UK = 23;

//error types
int EXPCT = 1;
int EUNXPCT = 2;

//error data
int excepParam = 0;
int lastExcep = 0;
int exCC = 0;
int exLC = 0;
int exLCC = 0;

// ===============

int findKW(){
    int nb_0 = 0;
    int i=0;
    int j = 0;
    int nbEqual = 0;
    while(nb_0 < NB_KW){
        
        if(keywords[i] == readBuffer[j]){
            nbEqual = nbEqual+1;
        }

        j = j+1;
        i = i+1;

        if(keywords[i] == 0){
            if(nbEqual == readBufferIndex & nbEqual == j){
                return nb_0;
            }
            nbEqual = 0;
            nb_0 = nb_0 + 1;
            j = 0;
            i = i + 1;
        }
        
        
    }
    return -1;
}

int cmpNames(int cmpOff){
    int j = 0;
    int o = IDOffset[cmpOff];
    while(j < readBufferIndex){
        if(IDs[o + j] != readBuffer[j]){
            return 0;
        }
        j = j+1;
    }
    int size = IDOffset[cmpOff+1] - o;
    return readBufferIndex == size;
}

int findID(){
    int i = 0;
    while(i < IDArrayPtr){
        if(cmpNames(i)){
            return i;
        }
        i = i+1;
    }
    return -1;
}


int addID(){
    int i=0;
    
    while(i < readBufferIndex){
        IDs[IDOffset[IDArrayPtr] + i] = readBuffer[i];
        i = i+1;
    }
    IDOffset[IDArrayPtr+1] = IDOffset[IDArrayPtr] + readBufferIndex;
    IDArrayPtr = IDArrayPtr + 1;
    
    return IDArrayPtr-1;
}


int cmpStr(int cmpOff){
    int j = 0;
    while(j < readBufferIndex){
        if(strings[cmpOff + j] != readBuffer[j]){
            return 0;
        }
        j = j+1;
    }
    return 1;
}

int findStr(){
    int i = 0;
    while(i < strArrayPtr){
        if(cmpNames(strOffset[i])){
            return i;
        }
        i = i+1;
    }
    return -1;
}


int addStr(){
    int i=0;
    
    while(i < readBufferIndex){
        strings[strOffset[strArrayPtr] + i] = readBuffer[i];
        strSize = strSize+1;
        i = i+1;
    }
    strOffset[strArrayPtr+1] = strOffset[strArrayPtr] + readBufferIndex;
    strArrayPtr = strArrayPtr + 1;
    
    return strArrayPtr-1;
}

int createID(char* name){
    int i=0;
    resetReadBuffer();
    while(name[i] != 0){
        readBufferAddChar(name[i]);
        i=i+1;
    }
    addID();
}

int tok;
int tokenValue;
int nextChar;

int read_int(){
    int ri_val = 0;
    while(num(nextChar)){
        int ri2 = ri_val + ri_val;
        int ri4 = ri2 + ri2;
        int ri10 = ri4 + ri4 + ri2;

        ri_val = ri10 + (nextChar - 48);
        nextChar = get();
        if(eof(nextChar)){
            return -1;
        }
    }
    return ri_val;
}
int read_quotes(){
    resetReadBuffer();
    nextChar = get();
    int escape = 0;
    int j = 0;
    while(escape | !quotes(nextChar)){
        if(eof(nextChar)){
            return -1;
        }
        
        if(!escape){ // blackslash
            if(nextChar == 92){
                escape = 1;
            }
            else{
                readBufferAddChar(nextChar);
            }
        }
        else{
            if(nextChar == 48){
                readBufferAddChar(0);
            }else if(nextChar == 110){
                readBufferAddChar(10);
            }
            else{
                readBufferAddChar(nextChar);
            }
            escape = 0;
        }
        nextChar = get();
    }
    readBufferAddChar(0);

    int tokenValue = findStr();
    if(tokenValue == -1){
        tokenValue = addStr();
    }
    return tokenValue;
}


int nextToken(){
    tokenValue = 0;

    while(blank(nextChar)){
        nextChar = get();
    }
    if(eof(nextChar)){
        return tok = TEND;
    }

    if(slash(nextChar)){
        nextChar = get();
        if(slash(nextChar)){
            while(!lf(nextChar)){
                nextChar = get();
            }
        } else {
            return tok = TERR;
        }
    }
    
    if(num(nextChar)){
        tokenValue = read_int(nextChar);
        if(tokenValue == -1){
            return tok = TERR;
        }
        return tok = TCONST;
    }
    else if(alpha(nextChar)){

        resetReadBuffer();
        while(alphanum(nextChar)){
            readBufferAddChar(nextChar);
            nextChar = get();
            if(eof(nextChar)){
                return tok = TERR;
            }
        }
        tokenValue = findKW();
        if(tokenValue == -1){
            tokenValue = findID();
            if(tokenValue == -1){
                tokenValue = addID();
            }
            return tok = TID;
        }
        else{
            return tok = TKW;
        }

    }
    else if(quotes(nextChar)){ //quote
    
        tokenValue = read_quotes();
        if(eof(nextChar)){
            return tok = TERR;
        }
        nextChar = get();
        return tok = TSTR;
        
    }
    else{
        if     (nextChar ==  59){tokenValue = TM_SC;} // ;
        else if(nextChar ==  44){tokenValue = TM_CM;} // ,

        else if(nextChar ==  40){tokenValue = TM_LP;} // (
        else if(nextChar ==  41){tokenValue = TM_RP;} // )
        
        else if(nextChar ==  91){tokenValue = TM_LB;} // [
        else if(nextChar ==  93){tokenValue = TM_RB;} // ]
        
        else if(nextChar == 123){tokenValue = TM_LC;} // {
        else if(nextChar == 125){tokenValue = TM_RC;} // }

        else if(nextChar ==  43){tokenValue = TM_PL;} // +
        else if(nextChar ==  45){tokenValue = TM_MN;} // -

        else if(nextChar == 124){tokenValue = TM_OR;} // |
        else if(nextChar ==  38){tokenValue = TM_ND;} // &

        else if(nextChar == 126){tokenValue = TM_CP;} // ~
        else if(nextChar ==  42){tokenValue = TM_ST;} // *

        else if(nextChar ==  33){                     // !
            nextChar = get();
            if(nextChar == 61){tokenValue = TM_NE;}   // !=
            else{
                tokenValue = TM_NT;
                return tok = TTERM;
            }
        }

        else if(nextChar == 61){                     // =
            nextChar = get();
            if(nextChar == 61){tokenValue = TM_EQ;}  // ==
            else{
                tokenValue = TM_AS;
                return tok = TTERM;
            }
        }

        else if(nextChar == 60){                     // <
            nextChar = get();
            if(nextChar == 61){tokenValue = TM_LE;}  // <=
            else{
                tokenValue = TM_LT;
                return tok = TTERM;
            }
        }

        else if(nextChar == 62){                     // >
            nextChar = get();
            if(nextChar == 61){tokenValue = TM_GE;}  // >=
            else{
                tokenValue = TM_GT;
                return tok = TTERM;
            }
        }
        else{
            tokenValue = TM_UK;
        }
        
        nextChar = get();
        return tok = TTERM;
    }
}


// (id, val)
int initAssigns[2048];
int initPtr = 0;

int addInit(int id, int val){
    initAssigns[initPtr] = id;
    initPtr=initPtr+1;
    initAssigns[initPtr] = val;
    initPtr=initPtr+1;
}

int outputPtr = 0;


int put(int value){
    write(value, outputPtr);
    outputPtr=outputPtr+1;
    return outputPtr-1;
}



int R0 = 0; int RRT = 0;
int R1 = 1;
int R2 = 2;
int R3 = 3; int RFP = 3;
int R4 = 4; int RSP = 4;
int R5 = 5; int RT1 = 5;
int R6 = 6; int RT2 = 6;
int R7 = 7; int RT3 = 7;
int R8 = 8; int RPI = 8;
int R9 = 9; int RPR = 9;

int NB_REG = 10;


int globalOffset = 0;
int constOffset = 0;
int strDataOffset = 0;
int regOffset = 0;
int initOffset = 0;
int inputOffset = 0;
int stackOffset = 0;
int stopAddress;

//(type, value, line)
int recordArray[98304];
int recordPtr = 0;


int mainID = 0;

int RT_REG = 0;
int RT_GLB = 1;
int RT_CST = 2;
int RT_STR = 3;
int RT_FUN = 4;
int RT_LBL = 5;


int record(int type, int value){
    recordArray[recordPtr] = type;
    recordPtr = recordPtr+1;
    recordArray[recordPtr] = value;
    recordPtr = recordPtr+1;
    recordArray[recordPtr] = -1;
    recordPtr = recordPtr+1;
    return -recordPtr;
}
int completeRecord(int addr, int line){
    recordArray[addr] = line;
}

int editRecord(int rec, int val){
    recordArray[-rec-2] = getConstant(val);
}

int writeRefs(){
    int i=0;
    int t;
    int v;
    int l;
    while(i < recordPtr){
        t = recordArray[i];
        v = recordArray[i+1];
        l = recordArray[i+2];
        
        if(t == RT_FUN){
            edit(functions[v + 3], l);
        }
        else if(t == RT_CST){
            edit(constOffset + v, l);
        }
        else if(t == RT_STR){
            edit(strDataOffset + strOffset[v], l);
        }
        else if(t == RT_GLB){
            edit(globals[v + 3], l);
        }
        else if(t == RT_REG){
            edit(regOffset + v, l);
        }
        else if(t == RT_LBL){
            edit(v, l);
        }else{
            return -1;
        }
        
        i=i+3;
    }
}

int cNOR = 0;
int cADD = 1073741824;
int cSTA = 2147483648;
int cJMP = 3221225472;

int iMKI(int inst, int addr){
    int val = addr & 1073741823;
    return inst | val;
}

int iNOR(int addr){return iMKI(cNOR, addr);}
int iADD(int addr){return iMKI(cADD, addr);}
int iSTA(int addr){return iMKI(cSTA, addr);}
int iJMP(int addr){return iMKI(cJMP, addr);}

int gINS(int inst, int addr){
    if(addr < 0){
        addr = -addr - 1;
        completeRecord(addr, outputPtr);
        return put(inst);
    }
    return put(iMKI(inst, addr));
}
int gNOR(int addr){return gINS(cNOR, addr);}
int gADD(int addr){return gINS(cADD, addr);}
int gSTA(int addr){return gINS(cSTA, addr);}
int gJMP(int addr){return gINS(cJMP, addr);}

int G_LEFTONE = 2147483648;
int G_ALLONES = 4294967295;

int gCST(int val){ //next opcode will get const addr
    return record(RT_CST, getConstant(val));
}

int gREG(int offset){ //next opcode will get reg addr
    return record(RT_REG, offset);
}

int gGLB(int id){ //next opcode will get global addr
    return record(RT_GLB, findGlobal(id));
}

int gFUN(int id){ //next opcode will get func addr
    return record(RT_FUN, findFunction(id));
}

int gSTR(int offset){ //next opcode will get str addr
    return record(RT_STR, offset);
}

int gLBL(int addr){ //next opcode will get str addr
    return record(RT_LBL, addr);
}

int gCLA(){ //clear accu
    gNOR(gCST(G_ALLONES));
}
int gCLC(){ //clear carry
    gADD(gCST(0));
}
int gCLR(){ //clear accu + carry
    gCLA();
    gCLC();
}

int gLDA(int addr){ // load addr to accu
    gCLA();
    gADD(addr);
}

int gCPL(){ // accu = ~accu
    gNOR(gCST(0));
}

int gAND(int addr){ //accu &= mem[addr]
    gCPL();
    gSTA(gREG(RT1));
    gLDA(addr);
    gCPL();
    gNOR(gREG(RT1));
}

int gBOR(int addr){ //accu |= mem[addr]
    gNOR(addr);
    gNOR(gCST(0));
}

int gMIN(int addr){ //accu -= mem[addr]
    gSTA(gREG(RT1));
    gCLA();
    gADD(addr);
    gCPL();
    gADD(gCST(1));
    gADD(gREG(RT1));
}

int gNOT(){
    int jStart = gJMP(0);
    int equal = outputPtr;
    gLDA(gCST(1));
    int jEnd = gJMP(0);
    
    int start = outputPtr;
    edit(start, jStart);
    gMIN(gCST(0));
    gADD(gCST(G_ALLONES));
    gJMP(equal);
    gCLA();

    edit(outputPtr, jEnd);
    gADD(gCST(0));
}

int gNEG(){ //accu = -accu
    gCPL();
    gADD(gCST(1));
}

int gCPY(int dst, int src){ //writes src to dst
    gLDA(src);
    gSTA(dst);
}

int gGNI(int dst, int inst, int ptr, int offset){ // perform inst on ptr value
    gLDA(inst);    //create inst
    int rec = gADD(ptr);   //configure inst with address
    gADD(offset);
    gSTA(dst);     //store generated instruction
    return rec;
}

int gSTP(int ptr){  // store accu to addr stored in ptr 
    gSTA(gREG(RT1));                     //save accu
    gGNI(gREG(RPI), iSTA(0), ptr, gCST(0)); //generate store
    int rec = gGNI(gREG(RPR), iJMP(0), 0, gCST(0));//generate jump back
    gLDA(gREG(RT1));                     //restore accu
    gJMP(gREG(RPI));                      //jump to generated inst
    edit(outputPtr, rec);
}
 
int gLDP(int ptr, int offset){  // load from addr stored in ptr
    gGNI(gREG(RPI), iADD(0), ptr, offset); //generate store
    int rec = gGNI(gREG(RPR), iJMP(0), 0, gCST(0));//generate store jump back
    gJMP(gREG(RPI));               //jump to generated inst
    edit(outputPtr, rec);
}

int gWPO(int addr, int offset){
    gLDA(gREG(RSP));    //get stack frame ptr
    gADD(offset);       //add offset
    gSTA(gREG(RT1));   //save stack address
    gLDA(addr);         //load value to store
    gSTP(gREG(RT1));   //store at address
}

int gRPO(int addr, int offset){
    gLDA(gREG(RFP));    //get stack frame ptr
    gADD(offset); //add offset
    gSTA(gREG(RT1));   //save stack address
    gLDP(gREG(RT1), gCST(0));  //load value
    gSTA(addr);   //store at address
}

int gISP(int offset){ //increment stack pointer
    gLDA(gREG(RSP));
    gADD(offset);
    gSTA(gREG(RSP));
}

int gPSH(int addr){ // push addr on stack
    gISP(gCST(1));       //increment stack pointer
    gLDA(addr);         //load value to store
    gSTP(gREG(RSP));    //store at address
}

int gPOP(int addr){ //pop stack into addr
    gLDP(gREG(RSP), gCST(0));
    gSTA(addr);
    gLDA(gCST(1));
    gNEG();
    gSTA(gREG(RT1));
    gISP(gREG(RT1));
}


int gPSF(int off){ //push stack frame
    //save registers
    gWPO(gREG(R1), gCST(1+off));
    gWPO(gREG(R2), gCST(2+off));
    gWPO(gREG(RFP), gCST(3+off));
    gWPO(gREG(RSP), gCST(4+off));
    
    //set R0 to 0
    gCLR();
    gSTA(gREG(R0));

    gISP(gCST(1));
    gCPY(gREG(RFP), gREG(RSP));
    gISP(gCST(3+off));
}

int gRSF(int off){ //pop stack frame
    gRPO(gREG(R1), gCST(1+off));
    gRPO(gREG(R2), gCST(2+off));
    gRPO(gREG(RSP), gCST(4+off));
    gRPO(gREG(RFP), gCST(3+off));
}

int gCLF(){
    //generate jump back
    gGNI(gREG(R0), iJMP(0), gREG(R0), gCST(0));
    gWPO(gREG(R0), gCST(1));
    gPSF(1);    
}

int gXTF(){
    gRPO(gREG(RT2), gCST(1));
    gRSF(1);
    gCLR();
    gJMP(gREG(RT2));
}

int gFNJ(int id){ // jump to function
    gCLC();
    gJMP(gFUN(id));
}

int gEXT(){
    gCLR();
    gJMP(stopAddress);
}

int gFill(){
    int i=0;
    constOffset = outputPtr;
    while(i < constantPtr){
        put(constants[i]);
        i=i+1;
    }

    i=0;    
    strDataOffset = outputPtr;
    while(i < strSize){
        put(strings[i]);
        i=i+1;
    }
}
int gPad(){
    int i=0;
    regOffset = outputPtr;
    while(i < NB_REG){
        put(0);
        i=i+1;
    }

    i=0;
    globalOffset = outputPtr;
    int off = globalOffset;
    while(i < globalPtr){
        int j=0;
        globals[i+3] = off;
        while(j < globals[i+1]){
            put(0);
            j=j+1;
        }
        off = off + j;
        
        i=i+4;
    }
    stackOffset = outputPtr;
}
int gInitGlobals(){
    initOffset = outputPtr;
    int i=0;
    while(i < initPtr){
        int id=initAssigns[i];
        int val=initAssigns[i+1];

        gCPY(gGLB(id), gCST(val));
         
        i=i+2;
    }
}


int gMain(){
    int addr = functions[mainID+3];
    if(addr == -1){
        return -1;
    }
    
    int rec;
    gCPY(gREG(R0), rec = gCST(0));
    gFNJ(mainID);
    editRecord(rec, outputPtr);
    gEXT();

}
int gWriteBack(){
    edit(initOffset, 0);
    //properties
    edit(inputOffset, 2); //input Offset
    stackOffset = inputOffset + peekOutput(5);
}



int nbInst = 0;

int pEnd(){
    return tok == TEND;
}
int pTerm(int val){
    return tok == TTERM & tokenValue == val;
}


int pId(){
    if(tok != TID){
        return -1;
    }
    return tokenValue;
}

int pType(){
    if(tok != TKW){
        return 0;
    }
    if(tokenValue == KW_INT){
        return TPINT;
    }

    if(tokenValue == KW_CHAR){
        nextToken();
        if(pTerm(TM_ST)){
            return TPSTR;
        }
        return -1;
    }
    return 0;
}
int pConst(){
    if(tok != TCONST){
        return -1;
    }
    return getConstant(tokenValue);
}

int pStr(){
    if(tok != TSTR){
        return -1;
    }
    return tokenValue;

}

int pExp();
int pOr(int id);

int pIf(int retAddr);
int pBody(int retAddr);


int pCallParam(int funcPtr, int param){
    int r;
    
    if(param==0 & pTerm(TM_RP)){
        return 0;
    }

    if((r = pExp()) < 1){
        return -1;
    }
    gPSH(gREG(RT1));
    
    if(pTerm(TM_RP)){
        return 1;
    }
    if(pTerm(TM_CM)){
        nextToken();
        if((r = pCallParam(funcPtr, param+1)) == -1){
            return -1;
        }
        return r+1;
    }
    return -1;
    
}
int pCall(int id){
    int r;
    int nbParam;
    int funcPtr = 0;
    if(pTerm(TM_LP)){
        funcPtr = findFunction(id);
        if(funcPtr == -1){
            return -1;
        }
        nextToken();
        gCLF();
        if((nbParam = pCallParam(funcPtr, 0)) == -1){
            return -1;
        }
        gFNJ(id);
        return 1;
    }
    return 0;
}
int pIndex(int id){
    int r;
    int varPtr = -1;
    int gPtr = -1;
    

    if(!pTerm(TM_LB)){
        return 0;
    }

    if((varPtr = findVariable(id)) == -1){
        if((gPtr = findGlobal(id)) == -1){
            return -1;
        }
    }
    int type;
    if(gPtr != -1){
        type = globals[gPtr+2];
        if(type == TPINT & globalSize(gPtr) < 2){
            return -1;
        }
    }
    if(varPtr != -1){
        type = variables[varPtr+2];
        if(type == TPINT & varSize(varPtr) < 2){
            return -1;
        }
    }

    nextToken();
    if(pExp() < 1){
        return -1;
    }

    if(!pTerm(TM_RB)){
        return -1;
    }
    nextToken();
    return 1;
}
int pExtra(int id){
    int r;
    if(r = pIndex(id)){
        return r;
    }
    if(r = pCall(id)){
        nextToken();
        return r;
    }
    return 0;
}
int pAssign(int id){
    int r;
    if(r = pIndex(id)){
        if(r == -1){
            return -1;
        }
    }
    if(pTerm(TM_AS)){
        nextToken();
        if((r = pExp()) < 1){
            return -1;
        }
        return 1;
    }
    return 0;
}
int pParExp(){
    if(!pTerm(TM_LP)){
        return 0;
    }
    nextToken();
    if(pExp() < 1){
        return -1;
    }
    //nextToken();
    if(!pTerm(TM_RP)){
        return -1;
    }
    nextToken();
    return 1;

}

int pPrimary(){
    int r;
    int id;
    if(r = pParExp()){
        return r;
    }
    if((r = pConst()) != -1){
        nextToken();
        return 1;
    }
    if((id = pId()) != -1){
        nextToken();
        if(r = pExtra(id)){
            if(r == -1){
                return -1;
            }
        }
        return 1;
    }
    return 0;
}

int pUnary(){
    
    if(pTerm(TM_MN)){
        nextToken();
    }
    else if(pTerm(TM_NT)){
        nextToken();
    }
    else if(pTerm(TM_CP)){
        nextToken();
    }
    else{
        int r;
        r = pPrimary();
        return r;
    }

    if(pPrimary() < 1){
        return -1;
    }
    return 1;
}

int pTrick(int id){
    int r;
    if(r = pAssign(id)){
        return r;
    }
    if(r = pOr(id)){
        return r;
    }
    return 0;
}
int pUntrick(int id){
    if(id == -1){
        return pUnary();
    }
    else{
        if(pExtra(id) == -1){
            return -1;
        }
        return 1;
    }
}
int pSum(int id){
    int r;
    if(r = pUntrick(id)){
        if(r == -1){
            return -1;
        }
        
        if(pTerm(TM_PL)){
            nextToken();
            if(pSum(-1) < 1){
                return -1;
            }
        }
        else if(pTerm(TM_MN)){
            nextToken();
            if(pSum(-1) < 1){
                return -1;
            }
        }
        return 1;
    }
    return 0;
}

int pDiff(int id){
    int r;
    if(r = pSum(id)){
        if(r == -1){
            return -1;
        }
        
        if(pTerm(TM_LT)){
            nextToken();
            if(pDiff(-1) < 1){
                return -1;
            }
        }
        if(pTerm(TM_GT)){
            nextToken();
            if(pDiff(-1) < 1){
                return -1;
            }
        }
        if(pTerm(TM_LE)){
            nextToken();
            if(pDiff(-1) < 1){
                return -1;
            }
        }
        if(pTerm(TM_GE)){
            nextToken();
            if(pDiff(-1) < 1){
                return -1;
            }
        }
        return 1;
    }
    return 0;
}

int pComp(int id){
    int r;
    if(r = pDiff(id)){
        if(r == -1){
            return -1;
        }
        
        if(pTerm(TM_EQ)){
            nextToken();
            if(pComp(-1) < 1){
                return -1;
            }
        }
        else if(pTerm(TM_NE)){
            nextToken();
            if(pComp(-1) < 1){
                return -1;
            }
        }
        return 1;
    }
    return 0;
}

int pAnd(int id){
    int r;
    if(r = pComp(id)){
        if(r == -1){
            return -1;
        }
        
        if(pTerm(TM_ND)){
            nextToken();
            if(pAnd(-1) < 1){
                return -1;
            }
        }
        return 1;
    }
    return 0;
}

int pOr(int id){
    int r;
    if(r = pAnd(id)){
        if(r == -1){
            return -1;
        }

        if(pTerm(TM_OR)){
            nextToken();
            if(pOr(-1) < 1){
                return -1;
            }
        }
        return 1;
    }
    return 0;
}

int pExp(){
    int r;
    int id;
    if((id = pId()) != -1){
        nextToken();
        if(pTrick(id) < 1){
            return -1;
        }
        return 1;
    }

    if(r = pOr(-1)){
        return r;
    }
    return 0;
}

int pLocalAssign(int id){
    int r;
    if(pTerm(TM_AS)){
        pushVariable(id, 1, TPINT);
        nextToken();
        if((r = pExp()) < 1){
            return -1;
        }
        return 1;
    }
    return 0;
}
int pLocalArray(int id){
    int cst;
    if(pTerm(TM_LB)){
        nextToken();
        if((cst = pConst()) == -1){
            return -1;
        }
        nextToken();
        if(!pTerm(TM_RB)){
            return -1;
        }
        pushVariable(id, constants[cst], TPINT);
        return 1;
    }
    return 0;
}

int pLocalStr(int id){
    int str;
    if(pTerm(TM_AS)){

        nextToken();
        if((str = pStr()) == -1){
            return -1;
        }
        pushVariable(id, 1, strOffset[str]);
        return 1;
    }
    return 0;
}
int pLocalInt(int id){
    int r;

    if((r = pAssign(id))){
        return r;
    }

    if(r = pLocalArray(id)){
        nextToken();
        return r;
    }

    pushVariable(id, 1, TPINT);
    return 1;
}
int pLocalVar(){
    int type;
    int id;

    if(type = pType()){
        if(type == -1){
            return -1;
        }
    }
    else{
        return 0;
    }
    
    nextToken();
    if((id = pId()) == -1){
        return -1;
    }
    
    nextToken();
    if(type == TPINT){
        return pLocalInt(id);
    }
    if(type == TPSTR){
        return pLocalStr(id);
    }
    
    return -1;
}

int pElse(int retAddr){
    int r;
    if(tok == TKW & tokenValue == KW_ELSE){
        nextToken();
        if(r = pIf(retAddr)){
            if(r == -1){
                return -1;
            }

            return 1;
        }
        if(r = pBody(retAddr)){
            if(r == -1){
                return -1;
            }
            nextToken();
            return 1;
        }
        return -1;

    }
    return 0;
}
int pIf(int retAddr){
    int r;
    if(tok == TKW & tokenValue == KW_IF){
        nextToken();
        if((r = pParExp()) < 1){
            return -1;
        }

        if((r = pBody(retAddr)) < 1){
            return -1;
        }

        nextToken();
        if(pElse(retAddr) == -1){
            return -1;
        }
        return 1;
    }
    return 0;
}

int pWhile(int retAddr){
    int r;
    if(tok == TKW & tokenValue == KW_WHILE){
        nextToken();
        if((r = pParExp()) < 1){
            return -1;
        }

        if((r = pBody(retAddr)) < 1){
            return -1;
        }
        return 1;
    }
    return 0;
}

int pReturn(int retAddr){
    int r;
    if(tok == TKW & tokenValue == KW_RETURN){
        nextToken();
        if(r = pExp() < 1){
            return -1;
        }
        gSTA(gREG(R0));
        gJMP(retAddr);
        return 1;
    }
    return 0;
}

int pStatement(){
    int id;
    int r;
    if((id = pId()) != -1){
        nextToken();
        if(r = pCall(id)){
            nextToken();
            return r;
        }
        if(r = pAssign(id)){
            return r;
        }
        return -1;
    }
    return 0;
}
int pInst(int retAddr, int breakAddr){
    nbInst = nbInst+1;
    int r;
    if(r = pLocalVar()){
        if(r == -1){
            return -1;
        }
    }
    else if(r = pStatement()){
        if(r == -1){
            return -1;
        }
    }
    else if(r = pReturn(retAddr)){
        if(r == -1){
            return -1;
        }
    }
    else if(r = pWhile(retAddr)){
        nextToken();
        return r;
    }
    else if(r = pIf(retAddr)){
        return r;
    }
    else{
        nbInst = nbInst-1;
        return 0;
    }

    if(pTerm(TM_SC)){
        nextToken();
        return 1;
    }
    return -1;
}

int pBodyProg(int retAddr, int breakAddr){
    int r;
    if(r = pInst(retAddr, breakAddr)){
        if(r == -1){
            return -1;
        }
        
        if(pBodyProg(retAddr, breakAddr) == -1){
            return -1;
        }
        return 1;
    }
    return 0;
}

int pBody(int retAddr){
    if(!pTerm(TM_LC)){
        return 0;
    }
    int r;

    int rec = gLBL(0);
    gPSF(0);

    nextToken();
    if(r = pBodyProg(retAddr, rec)){
        if(r == -1){
            return -1;
        }

    }
    
    if(!pTerm(TM_RC)){
        return -1;
    }

    gRSF(0);

    return 1;
}

int pFuncParam(int nParams, int funcPtr){
    int type;
    int id;
    int r;

    if(type = pType()){
        if(type == -1){
            return -1;
        }
        
        nextToken();
        if((id = pId()) == -1){
            return -1;
        }

        
        pushVariable(id, 1, type);
        if(funcPtr == -1){
            addFunctionParam(type, id);
        }
        else{
            int ptr = functions[funcPtr + 1];
            if(functionParams[ptr] != type | functionParams[ptr+1] != id){
                return -1;
            }
        }

        nextToken();

        if(pTerm(TM_RP)){
            return 1;
        }
        if(pTerm(TM_CM)){
            nextToken();
            if((r = pFuncParam(nParams+1, funcPtr)) < 1){
                return -1;
            }
            return r+1;
        }
        
        return -1;
    }
    if(nParams==0 & pTerm(TM_RP)){
        return 0;
    }
    
    return -1;
}

int gPAR(int funcPtr){
    int i = 0;
    int o = functions[funcPtr+1];
    int p;
    int type;
    int id;
    while(i < functions[funcPtr+2]){
        p = o + i+i;
        type = functionParams[p];
        id = functionParams[p+1]; 
        if(type == TPINT){
            gPSH(0);
        }
    }
}

int pDefineFunction(int funcPtr){
    int r;
    if(!pTerm(TM_LC)){
        return 0;
    }

    setFunctionCode(funcPtr, outputPtr);
    
    int rec = gLBL(0);

    nextToken();
    if(r = pBodyProg(rec, rec)){
        if(r == -1){
            return -1;
        }
    }

    if(!pTerm(TM_RC)){
        return -1;
    }

    editRecord(rec, outputPtr);
    gXTF();

    return 1;
}

int pFuncDeclaration(int id){
    if(!pTerm(TM_LP)){
        return 0;
    }
    if(findGlobal(id) != -1){
        return -1;
    }
    int f = findFunction(id);
    nextToken();

    int nbParams;
    if(f == -1){
        f = addFunction(id);

        restoreStack(0);
        nbParams = pFuncParam(0, -1);
        if(nbParams == -1){
            return -1;
        }
        setFunctionNbParam(f, nbParams);
        nextToken();
        
        int def;
        if(def = pDefineFunction(f)){
            if(def == -1){
                return -1;
            }
            return 1;
        }
        
        if(pTerm(TM_SC)){
            return 1;
        }
        return -1;
    }
    else{
        nbParams = pFuncParam(0, f);
        
        if(nbParams == -1 | functions[f+2] != nbParams){
            return -1;
        }

        nextToken();
        
        int def;
        if(def = pDefineFunction(f)){
            if(def == -1){
                return -1;
            }
            return 1;
        }

        return -1;
    }
}

int pGlobalArray(int id){
    int cst;
    if(pTerm(TM_LB)){
        nextToken();
        if((cst = pConst()) == -1){
            return -1;
        }
        nextToken();
        if(!pTerm(TM_RB)){
            return -1;
        }
        nextToken();
        if(!pTerm(TM_SC)){
            return -1;
        }
        if(findGlobal(id) != -1){
            return -1;
        }
        addGlobal(id, constants[cst], TPINT);
        return 1;
    }
    return 0;
}

int pInitAssign(int id){
    int cst;
    if(pTerm(TM_AS)){
        if((findGlobal(id)) != -1){
            return -1;
        }
        addGlobal(id, 1, TPINT);
        nextToken();
        if((cst = pConst()) == -1){
            return -1;
        }
        nextToken();
        if(!pTerm(TM_SC)){
            return -1;
        }
        addInit(id, constants[cst]);
        return 1;
    }
    return 0;
}

int pGlobalInt(int id){
    int r;
    
    if(pTerm(TM_SC)){
        if(findGlobal(id) != -1){
            return -1;
        }
        if(findFunction(id) != -1){
            return -1;
        }
        addGlobal(id, 1, TPINT);
        return 1;
    }

    if((r = pInitAssign(id))){
        return r;
    }

    if(r = pFuncDeclaration(id)){
        return r;
    }

    if(r = pGlobalArray(id)){
        return r;
    }

    return -1;
}

int pGlobalStr(int id){
    int str;
    if(pTerm(TM_AS)){
        if((findGlobal(id)) != -1){
            return -1;
        }
        if(findFunction(id) != -1){
            return -1;
        }
        addGlobal(id, 1, TPSTR);

        nextToken();
        if((str = pStr()) == -1){
            return -1;
        }

        nextToken();
        if(!pTerm(TM_SC)){
            return -1;
        }
        addInit(id, strOffset[str]);
        return 1;
    }
    return 0;
}

int pGlobalDeclaration(){
    int type;
    int id;

    if(type = pType()){
        if(type == -1){
            return -1;
        }
    }
    else{
        return 0;
    }
    
    nextToken();
    if((id = pId()) == -1){
        return -1;
    }
    
    nextToken();
    if(type == TPINT){
        return pGlobalInt(id);
    }
    if(type == TPSTR){
        return pGlobalStr(id);
    }
    
    return -1;
}

int pProg(){
    int c;
    int c2;
    if(c = pGlobalDeclaration()){
        if(c==-1){
            return -1;
        }
    }
    else{
        return 1;
    }

    nextToken();
    if((c2 = pProg()) == -1){
        return -1;
    }

    return 1;
}

int parse(){
    nextChar = get();
    nextToken();
    return pProg();
}

int compile(){
    mainID = createID("main");
    int i=0;

    put(gJMP(0)); //jmp to init  #0

    //properties
    put(4); //param Offset       #1
    put(0); //input Offset       #2
    put(0); //stop addr          #3
    
    //params
    put(0); // output Offset     #4
    put(0); // input size        #5
    
    //stop jmp
    edit(3, stopAddress = gJMP(outputPtr));

    parse();

    gFill();
    gPad();
    writeRefs();
    gInitGlobals();
    gMain();
    gWriteBack();
    
    put(gJMP(peekOutput(3)));
    
}
int main(){
    int t=0;
    int r = parse();
    if(r == -1){
        return -1;
    }
    return 0;
}